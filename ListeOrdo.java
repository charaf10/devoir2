package modele.tda;
import java.util.Collection;

public class ListeOrdo {


    public static final int Taille_parDefaut = 10;

    private Object[] tab;
    private int nbElements = 0;

    public ListeOrdo(){
        tab = new Object[Taille_parDefaut];
    }

    public ListeOrdo(int taille){
        tab = new Object[taille];
    }

    public void ajouter(int numConnexion, Object connexion) throws Exception{

        if(numConnexion < 0 || numConnexion > nbElements){
            //throw new Exception("[File.java] index invalide: " + numConnexion);
        }

        ajusterTailleTableau();

        decalageVersHaut(numConnexion);

        tab[numConnexion] = connexion;
        nbElements ++;
        System.out.println("le nombre d'element " + nbElements);

    }

    public void enlever(int numConnexion) throws Exception{

        ajusterTailleTableau();

        decalageVersHaut(numConnexion);

        tab[numConnexion] = 0;
        nbElements --;
        System.out.println("le nombre d'element " + nbElements);
    }

    public boolean afficher(int numConnexion) throws Exception{

        System.out.println(tab[numConnexion]);
        return true;
    }


    private void ajusterTailleTableau(){

        if(nbElements == tab.length){
            Connexion[] nouveau = new Connexion[tab.length*2];
            System.arraycopy(tab,0,nouveau,0,nbElements);
            tab = nouveau;
        }

    }

    private void decalageVersHaut(int index){

        for(int i=nbElements-1;i>=index;i--){
            tab[i+1] = tab[i];
        }
    }



}
