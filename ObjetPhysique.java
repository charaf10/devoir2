package modele.physique;

import java.lang.reflect.Constructor;

public abstract class ObjetPhysique {

    double positionX,positionY;

    Position position = new Position(positionX, positionY);

    public ObjetPhysique(double positionX, double positionY)
    {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public double getPositionX() { return positionX; }
    public double getPositionY() { return positionY; }

    public void setPositionX(double positionX) { this.positionX = positionX; }
    public void setPositionY(double positionY) { this.positionY = positionY; }
}
