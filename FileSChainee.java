package modele.tda;
    public class FileSChainee {

    public class Noeud{

        private Noeud suivant;
        private Object element;

        public Noeud(Noeud suivant, Object element){
            this.suivant = suivant;
            this.element = element;
        }
    }

    private Noeud tete = null;

    public void FileSchainee(){}

    public void enfiler(Object element){

        if(tete == null)
        {
            tete = new Noeud(null, element);
        }else{
            Noeud courant = tete;

            while (courant.suivant != null)
            {
                courant = courant.suivant;
            }
            courant.suivant = new Noeud(null, element);
        }
    }

    public String defiler()
    {
        if (tete == null)
        {
            return null;
        }
        Object element = tete.element;
        tete = tete.suivant;

        String ElementConvertToString = element.toString();

        return ElementConvertToString;
    }


    public String enleverAIndex(int index){

        if (tete == null)
        {
            return null;
        }

        Object element;

        if (index <= 0)
        {
            return defiler();
        }
        else if (tete.suivant == null)
        {
            element = tete.element;
            tete = null;
        }
        else
        {
            Noeud precedent = tete;
            Noeud courant = tete.suivant;
            int indexCourant = 1;

            while (indexCourant < index && courant.suivant != null)
            {
                precedent = courant;
                courant = courant.suivant;
                indexCourant++;
                System.out.println("Index courant = " + indexCourant);
            }

            element = courant.element;

            precedent.suivant = courant.suivant;

        }
        String retour = ConvertToString(element);
        return retour;
    }



    public void afficherContenu()
    {

        if (tete == null)
        {
            System.out.println("Liste vide");
        }
        else
        {
            Noeud courant = tete;
            System.out.println(courant.element);

            while (courant.suivant != null)
            {
                courant = courant.suivant;
                System.out.println(courant.element);
            }

        }
    }


    public String ConvertToString(Object ToConvert)
    {
        String ToString;
        ToString = ToConvert.toString();
        return ToString;
    }

}
