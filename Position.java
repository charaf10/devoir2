package modele.physique;
import java.math.*;
import javafx.geometry.Pos;

import java.util.ArrayList;
import java.util.Random;
import java.util.Random.*;

public class Position {

    double positionX, positionY;

    public double getPositionX() { return positionX; }
    public double getPositionY() { return positionY; }

    public void setPositionX(double positionX) { this.positionX = positionX; }
    public void setPositionY(double positionY) { this.positionY = positionY; }

    public Position(double positionX, double positionY)
    {

        this.positionX = positionX;
        this.positionY = positionY;

       // Position position = new Position(positionX,positionY);

    }

    public float distance2Point(Position pos1, Position pos2)
    {
        double distance;
        Position p1,p2;

        p1 = pos1;
        p2 = pos2;

        double exp1 = Math.pow((p2.positionX - p1.positionX) , 2);
        double exp2 = Math.pow((p2.positionY - p1.positionY), 2);

        distance = Math.sqrt(exp1+exp2);
        return ((float) distance);
    }



    public String ConvertToString(Object ToConvert)
    {
        String ToString;
        ToString = ToConvert.toString();

        return ToString;
    }


}
